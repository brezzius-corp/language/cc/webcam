#ifndef __HTTP_H
#define __HTTP_H

#include "constants.h"

#define HEADER_LENGTH_SIZE 16

typedef struct {
    char clength[BUFFER_SIZE];
    int length;
} HTTP_Header;

#endif /* __HTTP_H */

