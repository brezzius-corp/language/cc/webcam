#ifndef __SSL_H
#define __SSL_H

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "constants.h"

typedef struct {
    SSL *ssl;
    SSL_CTX *ctx;
    SOCKET server;
    const SSL_METHOD *method;
} SSL_C;

void SSL_Init();
SSL_C *SSL_Open(char *hostname);
void SSL_Close(SSL_C *ssl);
SOCKET SSL_Socket(char *hostname, int PORT);

#endif /* __SSL_H */

