#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#define INVALID_SCOKET -1
#define SOCKET_ERROR -1

#define PORT_HTTP 80
#define PORT_HTTPS 443
#define BUFFER_SIZE 1024

#define h_addr h_addr_list[0]

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;
typedef struct hostent HOSTENT;
typedef struct timeval TIMEVAL;

#endif /* __CONSTANTS_H */

