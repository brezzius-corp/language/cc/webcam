#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "constants.h"
#include "config.h"
#include "http.h"
#include "ssl.h"
#include "utils.h"

void getData(char *hostname, int type, int size, char *domain) {
    /* Main Variables */
    DIR *dir;
    FILE *f1, *f2;
    char buffer[BUFFER_SIZE];
    int c, i, l, n, p, byte;

    /* Socket Variables */
    SSL_C *ssl;

    /* HTTP Variables */
    HTTP_Header hd;

    /* Times Variables */
    char date[11], filename[10], skaping[17], tmp[1024], *path, pathLast[255];
    char suffixPrames[32];
    time_t timestamp;
    struct tm t;

    SSL_Init();

    for(p = 1 ; p <= size ; p++) {
        /* Open file */

        /* Get current time for image name */
        time(&timestamp);
        t = *localtime(&timestamp);
        strftime(date, 11, "%Y_%m_%d", &t);
        strftime(filename, 10, "%H_%M.", &t);

        /* Get time for skaping cameras */
        timestamp -= 300;
        t = *localtime(&timestamp);
        strftime(skaping, 17, "%Y/%m/%d/%H-%M", &t);

        if(type == 2 || type == 4 || type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 10)
            strcat(filename, "jpg");
        else if(type == 3)
            strcat(filename, "mp4");
    
        if(!(path = malloc(strlen(DIRNAME) + strlen(date) + strlen(filename) + 40))) {
            fprintf(stderr, "Memory error\n");
            exit(EXIT_FAILURE);
        }

        sprintf(pathLast, "%s/image/%s/LAST/", DIRNAME, domain);
        if(!(dir = opendir(pathLast)))
            UTL_Mkdir(pathLast, 0755);
    
        if(type == 4 || type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 10) {
            sprintf(path, "%s/image/%s/PRESET_%.2d/%s/", DIRNAME, domain, p, date);
            sprintf(tmp, "PRESET_%.2d.jpg", p);
            strcat(pathLast, tmp);
        } else if(type == 2) {
            sprintf(path, "%s/image/%s/PANORAMIC/%s/", DIRNAME, domain, date);
            strcat(pathLast, "PANORAMIC.jpg");
        } else if(type == 3) {
            sprintf(path, "%s/image/%s/%s/", DIRNAME, domain, date);
            strcat(pathLast, "VIDEO.mp4");
        }

        if(!(dir = opendir(path)))
            UTL_Mkdir(path, 0755);
    
        strcat(path, filename);
    
        /* Socket - Create */
        if(type == 2) {
            if(!strcmp(domain, "crabioules"))
                sprintf(buffer, "GET /luchon/plateau/panojavascript/pano_PV.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else
                sprintf(buffer, "GET /luchon/%s/panojavascript/pano_PV.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", domain, hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;

        } else if(type == 3) {
            sprintf(buffer, "GET /luchon/fixes/img/upload/%s.mp4 HTTP/1.1\r\nHost: %s\r\n\r\n", domain, hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;

        } else if(type == 4) {
            if(!strcmp(domain, "renclusa") && p == 1)
                sprintf(suffixPrames, "renclusa");
            else if(!strcmp(domain, "renclusa") && p == 2)
                sprintf(suffixPrames, "renclusab");
            else if(!strcmp(domain, "estos") && p == 1)
                sprintf(suffixPrames, "estos");
            else if(!strcmp(domain, "estos") && p == 2)
                sprintf(suffixPrames, "Estos000M");
            else if(!strcmp(domain, "bachimana") && p == 1)
                sprintf(suffixPrames, "bachimaña_n");
            else if(!strcmp(domain, "bachimana") && p == 2)
                sprintf(suffixPrames, "bachimaña_s");
            else if(!strcmp(domain, "respomuso") && p == 1)
                sprintf(suffixPrames, "respomuso");
            else if(!strcmp(domain, "respomuso") && p == 2)
                sprintf(suffixPrames, "respomuso_2");
            else if(!strcmp(domain, "goriz") && p == 1)
                sprintf(suffixPrames, "goriz");
            else if(!strcmp(domain, "goriz") && p == 2)
                sprintf(suffixPrames, "goriz2");
            else if(!strcmp(domain, "angel_orus") && p == 1)
                sprintf(suffixPrames, "angel_orus");
            else if(!strcmp(domain, "angel_orus") && p == 2)
                sprintf(suffixPrames, "Angel_Urus_b_000M");
            else if(!strcmp(domain, "linza") && p == 1)
                sprintf(suffixPrames, "linza");
            else if(!strcmp(domain, "linza") && p == 2)
                sprintf(suffixPrames, "linza2");

            sprintf(buffer, "GET /centralreservas/webrefugios/camaras/%s.jpg HTTP/1.1\r\nHost: %s\r\nReferer: %s\r\n\r\n", suffixPrames, hostname, refererPrames);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                continue;
        } else if(type == 5) {
            sprintf(buffer, "GET /webcams/4/2000/70/1182187370.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;
        } else if(type == 6) {
            if(!strcmp(domain, "tourmalet"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/tourmalet.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "merens"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/merens.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "pourtalet"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/pourtalet.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "gourette-cirque"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/gourette-cirque.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "gavarnie"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/gavarnie.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "saintlary"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/saintlary.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "soulor"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/soulor.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "couraduque"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/couraduque.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
            else if(!strcmp(domain, "prades"))
                sprintf(buffer, "GET /live/modules/timelapse/capture/prades.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;
        } else if(type == 7) {
            sprintf(buffer, "GET /ibox/ftpcam/mega_les-cortalets_refuge-des-cortalets.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;
        } else if(type == 8) {
            if(!strcmp(domain, "cauterets-lys"))
                sprintf(buffer, "GET /cauterets/cirque-du-lys/panorama/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "cauterets-pont-espagne"))
                sprintf(buffer, "GET /cauterets/pontdespagne/video/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "cauterets-village"))
                sprintf(buffer, "GET /cauterets/village/casino/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 1)
                sprintf(buffer, "GET /sommet-cap-de-pales/panoramique/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 2)
                sprintf(buffer, "GET /peyragudes/cap-de-pales/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 3)
                sprintf(buffer, "GET /peyragudes/serre-doumenge/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 4)
                sprintf(buffer, "GET /peyragydes/serias/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 5)
                sprintf(buffer, "GET /peyragudes/les-agudes/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "peyragudes") && p == 6)
                sprintf(buffer, "GET /peyragudes/peyresourde/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);
            else if(!strcmp(domain, "loudenvielle"))
                sprintf(buffer, "GET /loudenvielle/tour-de-genos/%s.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", skaping, hostname);

            /* Secure Connexion */
            if(!(ssl = SSL_Open(hostname)))
	    	continue;

            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                continue;

            if(strstr(buffer, "301 Moved Permanently"))
                continue;
            else {
                path[strlen(path) - 5] = skaping[strlen(skaping) - 1];
                path[strlen(path) - 6] = skaping[strlen(skaping) - 2];
                path[strlen(path) - 8] = skaping[strlen(skaping) - 4];
                path[strlen(path) - 9] = skaping[strlen(skaping) - 5];
            }
        } else if(type == 9) {
            if(!strcmp(domain, "summit_station") && p == 1)
                sprintf(buffer, "GET /data/insite/var/images/summitcamp/bighouse-netcam.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
	    else if(!strcmp(domain, "summit_station") && p == 2)
                sprintf(buffer, "GET /data/insite/var/images/summitcamp/smg-netcam.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
	    else if(!strcmp(domain, "summit_station") && p == 3)
                sprintf(buffer, "GET /data/insite/var/images/summitcamp/fuelpits-netcam.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;
        } else if(type == 10) {
            if(!strcmp(domain, "bielsa") && p == 1)
                sprintf(buffer, "GET /webcams/FR/FRANCIA.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);
	    else if(!strcmp(domain, "bielsa") && p == 2)
                sprintf(buffer, "GET /webcams/ESP/ESPANA.jpg HTTP/1.1\r\nHost: %s\r\n\r\n", hostname);

            /* Secure Connexion */
            ssl = SSL_Open(hostname);
            SSL_write(ssl->ssl, buffer, strlen(buffer));
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) <= 0)
                return;
	}
    
        byte = n;
        buffer[n] = '\0';
    
        for(c = 0 ; buffer[c] != '\r' || buffer[c+1] != '\n' || buffer[c+2] != '\r' || buffer[c+3] != '\n' ; c++);
        c+=4;
    
        if(!strstr(buffer, "Content-Length"))
            return;

        strcpy(hd.clength, strstr(buffer, "Content-Length") + 16);
        for(i = 0, l = strlen(hd.clength) ; i < l ; i++)
            if(hd.clength[i] == '\r' && hd.clength[i+1] == '\n')
                hd.clength[i] = '\0';
    
        hd.length = atoi(hd.clength) + c;
    
        if(!(f1 = fopen(path, "wb"))) {
            fprintf(stderr, "Cannot open file %s for write\n", path);
            return;
        }

        for( ; c < n ; c++)
            fwrite(&buffer[c], sizeof(char), 1, f1);
    
        while(byte < hd.length) {
            if((n = SSL_read(ssl->ssl, buffer, sizeof(buffer))) < 0)
                break;
    
            for(c = 0 ; c < n ; c++)
                fwrite(&buffer[c], sizeof(char), 1, f1);
    
            byte += n;
        }

        fclose(f1);

        /* Compare image */
        if(!(f1 = fopen(path, "rb"))) {
            fprintf(stderr, "Cannot open file %s for read\n", path);
            return;
        }

        /* Create last image if don't exist */
        if(!(f2 = fopen(pathLast, "rb")))
            fclose(fopen(pathLast, "w"));

        if(f1 && f2) {
            if(!(UTL_CompareFile(f1, f2)))
                remove(path);
            else {
                fclose(f2);
                f2 = fopen(pathLast, "wb");

                sprintf(tmp, "cp -f %s %s", path, pathLast);
                system(tmp);
            }
        }
        else {
            f2 = fopen(pathLast, "wb");

            sprintf(tmp, "cp -f %s %s", path, pathLast);
            system(tmp);
        }
    
        free(path);

        SSL_Close(ssl);
    }
}

int main(void) {
    printf("Crabioules\n");
    getData(hostnameNeo2, 2, 1, "crabioules");
    printf("Cecire\n");
    getData(hostnameNeo2, 2, 1, "cecire");
    printf("Lac Super\n");
    getData(hostnameNeo2, 3, 1, "lac_super");
    printf("Lac Cecire\n");
    getData(hostnameNeo2, 3, 1, "lac_cecire");
    printf("Rencluse\n");
    getData(hostnamePrames, 4, 2, "renclusa");
    printf("Estos\n");
    getData(hostnamePrames, 4, 2, "estos");
    printf("Bachimana\n");
    getData(hostnamePrames, 4, 2, "bachimana");
    printf("Respomuso\n");
    getData(hostnamePrames, 4, 2, "respomuso");
    printf("Goriz\n");
    getData(hostnamePrames, 4, 2, "goriz");
    printf("Anglet Orus\n");
    getData(hostnamePrames, 4, 2, "angel_orus");
    printf("Linza\n");
    getData(hostnamePrames, 4, 2, "linza");
    printf("Longyearbyen\n");
    getData(hostnameYr, 5, 1, "longyearbyen");
    printf("Tourmalet\n");
    getData(hostnameVision, 6, 1, "tourmalet");
    printf("Merens\n");
    getData(hostnameVision, 6, 1, "merens");
    printf("Pourtalet\n");
    getData(hostnameVision, 6, 1, "pourtalet");
    printf("Gourette Cirque\n");
    getData(hostnameVision, 6, 1, "gourette-cirque");
    printf("Gavarnie\n");
    getData(hostnameVision, 6, 1, "gavarnie");
    printf("Saint Lary\n");
    getData(hostnameVision, 6, 1, "saintlary");
    printf("Soulor\n");
    getData(hostnameVision, 6, 1, "soulor");
    printf("Couraduque\n");
    getData(hostnameVision, 6, 1, "couraduque");
    printf("Cortalets\n");
    getData(hostnameTrinum, 7, 1, "cortalets");
    printf("Cauterets Lys\n");
    getData(hostnameSkaping, 8, 1, "cauterets-lys");
    printf("Cauterets Pont d'Espagne\n");
    getData(hostnameSkaping, 8, 1, "cauterets-pont-espagne");
    printf("Cauterets Village\n");
    getData(hostnameSkaping, 8, 1, "cauterets-village");
    printf("Peyragudes\n");
    getData(hostnameSkaping, 8, 6, "peyragudes");
    printf("Loudenvielle\n");
    getData(hostnameSkaping, 8, 1, "loudenvielle");
    printf("Summit Station\n");
    getData(hostnameInsite, 9, 3, "summit_station");
    printf("Bielsa\n");
    getData(hostnameBielsa, 10, 2, "bielsa");
    printf("\nDone\n");

    return EXIT_SUCCESS;
}

