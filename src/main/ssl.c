#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "ssl.h"
#include "constants.h"

void SSL_Init() {
    OpenSSL_add_all_algorithms();
  
    if(SSL_library_init() < 0)
      fprintf(stderr, "Could not initialize the OpenSSL library\n");
}

SSL_C *SSL_Open(char *hostname) {
    SSL_C *ssl;

    if(!(ssl = malloc(sizeof(SSL_C)))) {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    ssl->method = SSLv23_client_method();
      
    ssl->ctx = SSL_CTX_new(ssl->method);
    
    SSL_CTX_set_options(ssl->ctx, SSL_OP_NO_SSLv2);
    
    ssl->ssl = SSL_new(ssl->ctx);
      
    if((ssl->server = SSL_Socket(hostname, PORT_HTTPS)) < 0)
        return NULL;
   
    SSL_set_fd(ssl->ssl, ssl->server);
     
    SSL_connect(ssl->ssl);

    return ssl;
}

void SSL_Close(SSL_C *ssl) {
    SSL_free(ssl->ssl);
    close(ssl->server);
    SSL_CTX_free(ssl->ctx);
}

SOCKET SSL_Socket(char *hostname, int PORT) {
    SOCKET sock;
    HOSTENT *host;
    SOCKADDR_IN dest_addr;
    TIMEVAL tv;
  
    if(!(host = gethostbyname(hostname)))
        return -1;
  
    sock = socket(AF_INET, SOCK_STREAM, 0);

    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(PORT);
    dest_addr.sin_addr.s_addr = *(long *) (host->h_addr);

    /* Set timeout */
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (TIMEVAL *) &tv, sizeof(TIMEVAL));
  
    if(connect(sock, (SOCKADDR *) &dest_addr, sizeof(SOCKADDR)) == -1)
        return -1;
  
    return sock;
}

