#ifndef __UTILS_H
#define __UTILS_H

void UTL_Mkdir(char *s, int mode);
int UTL_CompareFile(FILE *f1, FILE *f2);
void UTL_CopyFile(FILE *f1, FILE *f2);

#endif /* __UTILS_H */

