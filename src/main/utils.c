#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "utils.h"

void UTL_Mkdir(char *s, int mode) {
    struct stat st;
    char token[255], path[1024] = { 0 };
    int i, j, l;

    for(i = 1, j = 0, l = strlen(s) ; i < l ; i++) {
        if(s[i] == '/') {
            token[j] = '\0';
            j = 0;

            strcat(path, "/");
            strcat(path, token);

            if(stat(token, &st) != 0 || S_ISDIR(st.st_mode) == 0)
                mkdir(path, mode);
        } else
            token[j++] = s[i];
    }

    token[j] = '\0';

    strcat(path, "/");
    strcat(path, token);

    if(stat(token, &st) != 0 || S_ISDIR(st.st_mode) == 0)
        mkdir(path, mode);
}

int UTL_CompareFile(FILE *f1, FILE *f2) {
    char c1, c2;
    int f1ok, f2ok;

    f1ok = fread(&c1, sizeof(char), 1, f1);
    f2ok = fread(&c2, sizeof(char), 1, f2);

    while(f1ok && f2ok) {
        if((c1 ^ c2) != 0)
            return 1;

        f1ok = fread(&c1, sizeof(char), 1, f1);
        f2ok = fread(&c2, sizeof(char), 1, f2);
    }

    if(f1ok || f2ok)
        return 1;

    return 0;
}

void UTL_CopyFile(FILE *f1, FILE *f2) {
    uint8_t buffer[1024];
    int n;

    while((n = fread(buffer, sizeof(uint8_t), sizeof(buffer), f1)) > 0)
        fwrite(buffer, sizeof(uint8_t), n, f2);
}

