#ifndef __CONFIG_H
#define __CONFIG_H

char *DIRNAME = "/app/applis/webcam";

char *hostnameNeo2 = "live.neos360.com";
char *hostnamePrames = "prames.es";
char *hostnameYr = "www.yr.no";
char *hostnameVision = "s1.vision-environnement.com";
char *hostnameTrinum = "www.trinum.com";
char *hostnameSkaping = "data.skaping.com";
char *hostnameInsite = "insite.arsls.org";
char *hostnameBielsa = "bielsa-aragnouet.org";

char *refererPrames = "https://www.alberguesyrefugios.com/";

#endif /* __CONFIG */

