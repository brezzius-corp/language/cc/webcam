# Makefile

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
LDFLAGS=-lssl -lcrypto
EXECUTABLE=webcam
EXECUTABLE_TEST=webcam_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: webcam
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/ssl.o $(SRC_BIN)/utils.o $(LDFLAGS)

test: package webcam_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

webcam: init main.o ssl.o utils.o
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/ssl.o $(SRC_BIN)/utils.o $(LDFLAGS)

webcam_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/constants.h $(SRC_MAIN)/config.h $(SRC_MAIN)/http.h $(SRC_MAIN)/ssl.h $(SRC_MAIN)/utils.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

ssl.o: $(SRC_MAIN)/ssl.c $(SRC_MAIN)/ssl.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/ssl.o -c $(SRC_MAIN)/ssl.c $(CFLAGS)

utils.o: $(SRC_MAIN)/utils.c $(SRC_MAIN)/utils.h
	$(CC) -o $(SRC_BIN)/utils.o -c $(SRC_MAIN)/utils.c $(CFLAGS)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

